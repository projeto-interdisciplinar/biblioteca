#include <ctype.h>
#include <stdio.h>
#include <locale.h>
#include <stdio_ext.h>
#include <mysql/mysql.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//LIVRO =============================================================================================================================================================================

void cadastrar_livro(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    char sql[1000],titulo[60],editora[60],idioma[60],isbn[60],data_publicacao[60],cod_livro[60],autor[400];
    int conta;
    strcpy(sql,"INSERT INTO livro(titulo,editora,idioma,isbn,data_publicacao,autor) VALUES('");
    //Titulo
    printf("Digite o titulo:");
    fflush(stdin);
    fgets(titulo,1000,stdin);
    strcat(sql,titulo);
    strcat(sql, "','");

    //Editora
    printf("Digite a editora:");
    fflush(stdin);
    fgets(editora,1000,stdin);
    strcat(sql,editora);
    strcat(sql, "','");

    //Idioma
    printf("Digite o idioma:");
    fflush(stdin);
    fgets(idioma,1000,stdin);
    strcat(sql,idioma);
    strcat(sql, "','");

    //isbn
    printf("Digite o ISBN:");
    fflush(stdin);
    fgets(isbn,1000,stdin);
    strcat(sql,isbn);
    strcat(sql, "','");

    //data da publicação
    printf("Digite a data da publicacao(AAAA/MM/DD):");
    fflush(stdin);
    fgets(data_publicacao,1000,stdin);
    strcat(sql,data_publicacao);
    strcat(sql, "','");

    printf("Digite do autor:");
    fflush(stdin);
    fgets(autor,1000,stdin);
    strcat(sql,autor);
    
     strcat(sql, "');");

    mysql_query(&conexao,sql);
    //resp = mysql_store_result(&conexao);
    //campos = mysql_fetch_fields(resp);

    //pegando o codigo do ultimo livro inserido na tabela
   // mysql_query(&conexao, "select max(codigo) from livro;");
   // resp = mysql_store_result(&conexao);
    //linhas=mysql_fetch_row(resp);
   // cod_livro = linhas[0]; 

   // cadastrar_catlivro1(conexao, resp, linhas, campos,cod_livro);
}

void consultar_livro_nomeEcodEautor(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    char sql[500];
    int conta;
    strcpy(sql,"SELECT livro.codigo, livro.titulo, livro.autor FROM livro;");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf("  \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s         \t",linhas[conta]);
            fflush(stdin);
        }
        printf("\n");
    }
    mysql_free_result(resp);
}

void excluir_livro(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    consultar_livro_nomeEcodEautor(conexao, resp, linhas, campos);
    printf("\n");

    char sql[500],codigo[60];
    strcpy(sql,"DELETE from livro WHERE codigo=");
    printf("Digite o codigo do livro que irá ser excluido:");
    fflush(stdin);
    fgets(codigo,1000,stdin);
    strcat(sql,codigo);
    strcat(sql,";");
    mysql_query(&conexao,sql);
    //resp = mysql_store_result(&conexao);
    //campos = mysql_fetch_fields(resp);

    printf("\n");
    consultar_livro_nomeEcodEautor(conexao, resp, linhas, campos);

}

//CATEGORIA ========================================================================================================================================================================

void cadastrar_categoria(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
	char sql[500],descricao[60];
	strcpy(sql,"INSERT INTO categoria(descricao) VALUES('");
    printf("Digite o nome da categoria:");
    fflush(stdin);
    fgets(descricao,1000,stdin);
    strcat(sql,descricao);
    strcat(sql,"');");
    mysql_query(&conexao,sql);

	int cadastrar_denovo;
    printf("Deseja fazer cadastro de outra categoria?\n");
    printf("0-Sim 1-Nao\n");
    scanf("%i",&cadastrar_denovo);

    if(cadastrar_denovo == 0){
    	cadastrar_categoria(conexao, resp, linhas, campos);
    }else{
    	//função que chamasse o menu de volta
    }
}

void consultar_categoria(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
	char sql[500],descricao[60];
	int conta;
	strcpy(sql,"SELECT view_consulta_cat;");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf(" \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s\t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);
}

void atualizar_categoria(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
	//Consultar as categorias existentes para saber qual irá ser atualizada
	consultar_categoria(conexao, resp, linhas, campos);

	char sql[500],descricao[60],codigo[10];
	strcpy(sql,"UPDATE categoria set descricao='");
    printf("Digite a nova descricao da categoria:");
    fflush(stdin);
    fgets(descricao,1000,stdin);
    strcat(sql,descricao);
    strcat(sql,"' WHERE codigo=");
    printf("Digite o codigo da categoria que será atualizada:");
    fflush(stdin);
    fgets(codigo,1000,stdin);
    strcat(sql,codigo);
    strcat(sql,";");
    mysql_query(&conexao,sql);
    //resp = mysql_store_result(&conexao);
    //campos = mysql_fetch_fields(resp);
}

void excluir_categoria(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
	//Consultar as categorias existentes para saber qual irá ser excluida
	consultar_categoria(conexao, resp, linhas, campos);

	char sql[500],codigo[60];
	strcpy(sql,"delete from categoria where codigo=");
    printf("Digite o codigo da categoria que irá ser excluida:");
    fflush(stdin);
    fgets(codigo,1000,stdin);
    strcat(sql,codigo);
    strcat(sql,";");
    mysql_query(&conexao,sql);
    //resp = mysql_store_result(&conexao);
    //campos = mysql_fetch_fields(resp);
}

//FUNÇÕES DA TABELA CAT_LIVRO -> CATEGORIAS QUE O LIVRO POSSUI,SENDO QUE PODE SER MAIS DE UMA ======================================================================================
void cadastrar_catlivro(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    int conf=0;
    //cod_l = cod_livro;
    printf("Livros cadastrados:\n");
    consultar_livro_nomeEcodEautor(conexao, resp, linhas, campos);

    printf("\n");

    printf("Categorias cadastradas:\n");
    consultar_categoria(conexao, resp, linhas, campos);

    printf("\n");

    printf("Seu livro ou categoria estao cadastrados?\n");
    printf("0-Sim  1-Nao\n");
    scanf("%i",&conf);

    printf("\n");

    if(conf==0){
    char sql[500],cod_categoria[60],cod_book[60];
    strcpy(sql,"INSERT INTO cat_livro(cod_livro,cod_categoria) VALUES(");
    fflush(stdin);
    //sprintf(cod_livro,"%d",cod_book);
    fgets(cod_book,1000,stdin);
    strcat(sql,cod_book);
    strcat(sql,",");

    printf("\n");

    printf("Digite o codigo da categoria que sera add ao livro:");
    fflush(stdin);
    fgets(cod_categoria,1000,stdin);
    strcat(sql,cod_categoria);
    strcat(sql,";");

    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    }else{
      printf("Volte para o menu principal e cadastre o necessário antes!!\n");
         //goto main;
    }

    int cadastrar_denovo;
    printf("Deseja fazer cadastro de outra categoria em algum livro?\n");
    printf("0-Sim 1-Nao\n");
    scanf("%i",&cadastrar_denovo);

    if(cadastrar_denovo == 0){
        cadastrar_catlivro(conexao, resp, linhas, campos);
    }else{
        //função que chamasse o menu de volta
    }

}

void consultar_catlivro(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    char sql[500],descricao[60];
    int conta;
    strcpy(sql,"SELECT*FROM view_consulta_catlivro;");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf(" \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s\t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);
}

void excluir_catlivro(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    //Consultar as categorias existentes para saber qual irá ser excluida
    consultar_catlivro(conexao, resp, linhas, campos);

    char sql[500],codigo[60];
    strcpy(sql,"DELETE FROM cat_livro WHERE cat_livro.cod_categoria =");
    printf("Digite o codigo da categoria que irá ser excluida:");
    fflush(stdin);
    fgets(codigo,1000,stdin);
    strcat(sql,codigo);
    strcat(sql," AND cat_livro.cod_livro =");
    mysql_query(&conexao,sql);

    printf("Digite o codigo do livro");
    fflush(stdin);
    fgets(codigo,1000,stdin);
    strcat(sql,codigo);
    strcat(sql,";");
    mysql_query(&conexao,sql);
     
    //resp = mysql_store_result(&conexao);
    //campos = mysql_fetch_fields(resp);
}



//FUNCIONARIO============================================================================================================================================================

void cadastrar_funcionario(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    __fpurge(stdin);
    printf("CADASTRO DE FUNCIONARIO\n");

    char sql[500], nome[60], cpf[14],  data_nascimento[10], endereco[30], email[30], senha[30];
    strcpy(sql,"INSERT INTO funcionario(funcao,nome,cpf,data_nascimento,endereco,email,senha) VALUES(0,'");
    
    printf("NOME COMPLETO:");
    fflush(stdin);
    fgets(nome,1000,stdin);
    strcat(sql,nome);
    strcat(sql,"','");
    
    printf("CPF:");
    fflush(stdin);
    fgets(cpf,1000,stdin);
    strcat(sql,cpf);
    strcat(sql,"','");
    
    printf("DATA NASCIMENTO:");
    fflush(stdin);
    fgets(data_nascimento,1000,stdin);
    strcat(sql,data_nascimento);
    strcat(sql,"','");
    
    printf("ENDEREÇO:");
    fflush(stdin);
    fgets(endereco,1000,stdin);
    strcat(sql,endereco);
    strcat(sql,"','");
    
    printf("EMAIL:");
    fflush(stdin);
    fgets(email,1000,stdin);
    strcat(sql,email);
    strcat(sql,"','");
    
    printf("SENHA:");
    fflush(stdin);
    fgets(senha,1000,stdin);
    strcat(sql,senha);
    strcat(sql,"');");
    
    mysql_query(&conexao,sql);
}

void consultar_funcionario(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    int conta;
    char sql[100];
    strcpy(sql,"SELECT funcionario.codigo, funcionario.nome FROM funcionario;");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf(" \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s\t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);
}

void excluir_funcionario(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    consultar_funcionario(conexao, resp, linhas, campos);

    char sql[500],codigo[60];
    strcpy(sql,"delete from funcionario where codigo=");
    printf("Digite o codigo do funcionario que irá ser excluido:");
    fflush(stdin);
    fgets(codigo,1000,stdin);
    strcat(sql,codigo);
    strcat(sql,";");
    mysql_query(&conexao,sql);
}



//EXEMPLAR -=========================================================================================================================================================================

void cadastrar_exemplar(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}

void consultar_exemplar(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}

void excluir_exemplar(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}

//EMPRESTIMO -=========================================================================================================================================================================

void cadastrar_emprestimo(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}

void consultar_emprestimo(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}

void excluir_emprestimo(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}


//RENOVAÇÂO -=========================================================================================================================================================================

void cadastrar_renovacao(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}

void consultar_renovacao(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}

void excluir_renovacao(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

}


int main(){
    setlocale(LC_ALL,"");
    char sql[500];
    int i,conta;
    unsigned short int opcao;
    MYSQL conexao;
    MYSQL_RES *resp;
    MYSQL_ROW linhas;
    MYSQL_FIELD *campos;

    mysql_init(&conexao);
        if ( !mysql_real_connect(&conexao, "localhost", "root", "root", "Biblioteca_BBC", 0, NULL, 0) ){
            printf("Erro ao conectar com o DB...\n");
        }else{
           cadastrar_funcionario(conexao, resp, linhas, campos);
        }
}