#include <ctype.h>
#include <stdio.h>
#include <locale.h>
#include <stdio_ext.h>
#include <mysql/mysql.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "Funcionario.c"
//#include "Menu.c"

void consultar_livro_cat(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
	//Fazendo a consulta das categorias
	consultar_categoria(conexao, resp, linhas, campos);

	int cod_cat,conta;

	printf("Digite o codigo da categoria escolhida:\n");
	scanf("%i",&cod_cat);

	char sql[500],codigo[10];
	strcpy(sql,"SELECT livro.titulo as TITULOS FROM livro JOIN cat_livro ON(livro.codigo = cat_livro.cod_livro)  JOIN categoria ON(cat_livro.cod_categoria = categoria.codigo) WHERE cat_livro.cod_categoria=");
    fflush(stdin);
    sprintf(codigo,"%d",cod_cat);
    strcat(sql,codigo);
    strcat(sql,";");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf("    \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s   \t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);

    printf("\n");

    int consultar_denovo;
    printf("Deseja fazer outra consulta de livro por categoria?\n");
    printf("0-Sim 1-Nao\n");
    scanf("%i",&consultar_denovo);

    if(consultar_denovo == 0){
    	consultar_livro_cat(conexao, resp, linhas, campos);
    }else{
    	//menu_usuario(conexao, resp, linhas, campos);
    }
}

void consultar_livro_titulo(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

    int conta;
    char sql[100],titulo[60];
    strcpy(sql,"SELECT livro.editora,livro.autor,livro.data_publicacao FROM livro WHERE livro.titulo='");
    printf("Digite o titulo do livro desejado:\n");
    fflush(stdin);
    fgets(titulo,1000,stdin);
    strcat(sql,titulo);
    strcat(sql, "';");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    printf("\n");
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf("    \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s    \t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);

    printf("\n");

    int consultar_denovo;
    printf("Deseja fazer outra consulta por titulo?\n");
    printf("0-Sim 1-Nao\n");
    scanf("%i",&consultar_denovo);

    if(consultar_denovo == 0){
        consultar_livro_titulo(conexao, resp, linhas, campos);
    }else{
       //menu_usuario(conexao, resp, linhas, campos);
    }
}

void consultar_livro_autor(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){

    int conta;
    char sql[100],autor[60];
    strcpy(sql,"SELECT livro.editora,livro.titulo,livro.data_publicacao  FROM livro WHERE livro.autor='");
    printf("Digite o autor do livro desejado:\n");
    fflush(stdin);
    fgets(autor,1000,stdin);
    strcat(sql,autor);
    strcat(sql, "';");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf(" \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s\t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);

    int consultar_denovo;
    printf("Deseja fazer outra consulta por autor?\n");
    printf("0-Sim 1-Nao\n");
    scanf("%i",&consultar_denovo);

    if(consultar_denovo == 0){
        consultar_livro_autor(conexao, resp, linhas, campos);
    }else{
       // menu_usuario(conexao, resp, linhas, campos);
    }
}

void consultar_emprestimos_abertos(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
    int conta;
    char sql[100],autor[60];
    strcpy(sql,"SELECT emprestimo.data_emprestimo FROM emprestimo WHERE emprestimo.status_emprestimo = 0';");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf(" \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s\t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);

// menu_usuario(conexao, resp, linhas, campos);
}

void consultar_emprestimos_fechados(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos){
        int conta;
    char sql[100],autor[60];
    strcpy(sql,"SELECT emprestimo.data_emprestimo FROM emprestimo WHERE emprestimo.status_emprestimo = 1;");
    mysql_query(&conexao,sql);
    resp = mysql_store_result(&conexao);
    campos = mysql_fetch_fields(resp);
    for (conta=0;conta<mysql_num_fields(resp);conta++) {
        printf("%s",(campos[conta]).name);
        if (mysql_num_fields(resp)>1){
            printf(" \t");
        }
    }
    printf("\n");  
    while ((linhas=mysql_fetch_row(resp)) != NULL){
        for (conta=0;conta<mysql_num_fields(resp);conta++){
            printf("%s\t",linhas[conta]);
        }
        printf("\n");
    }
    mysql_free_result(resp);

// menu_usuario(conexao, resp, linhas, campos);
}



