#include <ctype.h>
#include <stdio.h>
#include <locale.h>
#include <stdio_ext.h>
#include <mysql/mysql.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//#include "Usuario.c"
//#include "Funcionario.c"

void menu_funcionario(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos,int id_funcionario){
    printf("Funcao menu funcionario\n");
    unsigned short int opcao1;
    __fpurge(stdin);;
    system("clear");
    int conta;
    printf("*========================FUNCIONARIO=======================*\n");
    printf("|                                                          |\n");
    printf("|  [CATEGORIA]  |   [LIVRO]   | [CAT_LIVRO] |  [EXEMPLAR]  |\n");
    printf("|               |             |             |              |\n");
    printf("| 1.CONSULTAR   | 5.CONSULTAR | 8.CONSULTAR | 11.CONSULTAR |\n");
    printf("| 2.CADASTRAR   | 6.CADASTRAR | 9.CADASTRAR | 12.CADASTRAR |\n");
    printf("| 3.ATUALIZAR   | 7.EXCLUIR   | 10.EXCLUIR  | 13.EXCLUIR   |\n");
    printf("| 4.EXCLUIR     |             |             |              |\n");
    printf("*==========================================================*\n");
    printf("|                                                          |\n");
    printf("|        [EMPRESTIMOS]         |       [RENOVACAO]         |\n");
    printf("|                              |                           |\n");
    printf("|        14.CONSULTAR          |        17.CONSULTAR       |\n");
    printf("|        15.CADASTRAR          |        18.CADASTRAR       |\n");
    printf("|        16.EXCLUIR            |        19.EXCLUIR         |\n");
    printf("|                              |                           |\n");
    printf("*==========================================================*\n");
    scanf("%hi", &opcao1);
    __fpurge(stdin);
    switch (opcao1){
        case 1:{}

        case 2:{}

        case 3:{}

        case 4:{}
    }
}

void menu_gerente(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos, int id_funcionario){
    printf("Funcao menu gerete\n");
    unsigned short int opcao1;
    __fpurge(stdin);;
    system("clear");
    int conta;
    printf("*==========================GERENTE=========================*\n");
    printf("|                                                          |\n");
    printf("|  [CATEGORIA]  |   [LIVRO]   | [CAT_LIVRO] |  [EXEMPLAR]  |\n");
    printf("|               |             |             |              |\n");
    printf("| 1.CONSULTAR   | 5.CONSULTAR | 8.CONSULTAR | 11.CONSULTAR |\n");
    printf("| 2.CADASTRAR   | 6.CADASTRAR | 9.CADASTRAR | 12.CADASTRAR |\n");
    printf("| 3.ATUALIZAR   | 7.EXCLUIR   | 10.EXCLUIR  | 13.EXCLUIR   |\n");
    printf("| 4.EXCLUIR     |             |             |              |\n");
    printf("*==========================================================*\n");
    printf("|                                             |\n");
    printf("| [EMPRESTIMOS] | [RENOVACAO]  |[FUNCIONARIO] |\n");
    printf("|               |              |              |\n");
    printf("| 14.CONSULTAR  | 17.CONSULTAR | 20.CONSULTAR |\n");
    printf("| 15.CADASTRAR  | 18.CADASTRAR | 21.CADASTRAR |\n");
    printf("| 16.EXCLUIR    | 19.EXCLUIR   | 22.EXCLUIR   |\n");
    printf("|               |              |              |\n");
    printf("*=============================================*\n");
    scanf("%hi", &opcao1);
    __fpurge(stdin);
    switch (opcao1){
        case 1:{}

        case 2:{}

        case 3:{}

        case 4:{}
    }
}

void menu_usuario(MYSQL conexao, MYSQL_RES *resp, MYSQL_ROW linhas, MYSQL_FIELD *campos, int id_usuario){
    printf("Funcao menu usuario\n");
    unsigned short int opcao1;
    __fpurge(stdin);;
    system("clear");
    int conta;
    printf("*==================CLIENTE==============*\n");
    printf("|                     |                 |\n");
    printf("|  [PESQUISAR LIVRO]  |   [EMPRESTIMOS] |\n");
    printf("|                     |                 |\n");
    printf("| 1.TITULO            | 5.ABERTOS       |\n");
    printf("| 2.AUTOR             | 6.FECHADOS      |\n");
    printf("| 3.CATEGORIA         |                 |\n");
    printf("*=======================================*\n");
    scanf("%hi", &opcao1);
    __fpurge(stdin);
    switch (opcao1){
        case 1:{}

        case 2:{}

        case 3:{}

        case 4:{}
    }

}