CREATE DATABASE Biblioteca_BBC;

USE Biblioteca_BBC;

CREATE TABLE usuario(
codigo integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
nome varchar(30) NOT NULL,
cpf varchar(14) NOT NULL,
data_nascimento date NOT NULL,
endereco varchar(300) NOT NULL,
email  varchar(60) NOT NULL,
senha varchar(6) NOT NULL);

CREATE TABLE funcionario(
codigo integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
nome varchar(30) NOT NULL,
cpf varchar(14) NOT NULL,
data_nascimento date NOT NULL,
endereco varchar(50) NOT NULL,
email  varchar(60) NOT NULL,
funcao int NOT NULL, -- 0 funcionario normal e 1 gerente
senha varchar(6) NOT NULL);

CREATE TABLE emprestimo(
codigo integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
status_emprestimo varchar(7) NOT NULL, 
data_emprestimo date NOT NULL,
cod_usuario integer NOT NULL,
cod_funcionario integer NOT NULL,
CONSTRAINT fk_emprestimo_usuario FOREIGN KEY (cod_usuario) REFERENCES usuario(codigo),
CONSTRAINT fk_emprestimo_funcionario FOREIGN KEY (cod_funcionario) REFERENCES funcionario(codigo)); 

CREATE TABLE categoria(
codigo integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
descricao varchar(30) NOT NULL);

/*CREATE TABLE autores(
codigo integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
nome varchar(30) NOT NULL); */

CREATE TABLE livro(
codigo integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
titulo integer NOT NULL,
editora varchar(30) NOT NULL,
idioma varchar(30) NOT NULL,
isbn varchar(50) NOT NULL,
data_publicacao datetime NOT NULL,
autor varchar(400) NOT NULL);

CREATE TABLE cat_livro(
cod_categoria integer NOT NULL,
cod_livro integer NOT NULL,
CONSTRAINT fk_catlivro_categoria FOREIGN KEY (cod_categoria) REFERENCES categoria(codigo),
CONSTRAINT fk_catlivro_livro FOREIGN KEY (cod_livro) REFERENCES livro(codigo),
PRIMARY KEY (cod_livro, cod_categoria));

/*CREATE TABLE livro_autores(
cod_livro integer NOT NULL,
cod_autores integer NOT NULL,
CONSTRAINT fk_livro FOREIGN KEY (cod_livro) REFERENCES livro(codigo),
CONSTRAINT fk_autores FOREIGN KEY (cod_autores) REFERENCES autores(codigo),
PRIMARY KEY(cod_livro, cod_autores));*/

CREATE TABLE exemplares(
codigo integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
status_livro tinyint(0) NOT NULL,/*0 = livre,1 = ocupado*/
cod_livro integer NOT NULL,
CONSTRAINT fk_exemplar_livro FOREIGN KEY (cod_livro) REFERENCES livro(codigo));

CREATE TABLE exemplares_emprestimo(
data_prev_devolucao date NULL,
data_dev_definitiva date NULL,
data_renovacao date NULL,
cod_exemplares integer NOT NULL,
cod_emprestimo integer NOT NULL,
CONSTRAINT fk_exemplares FOREIGN KEY (cod_exemplares) REFERENCES exemplares(codigo),
CONSTRAINT fk_emprestimo FOREIGN KEY (cod_emprestimo) REFERENCES emprestimo(codigo),
PRIMARY KEY (cod_exemplares, cod_emprestimo)); 

-- insert into autores(nome) VALUES('Isabella'); 
select max(codigo) from livro;
select max(codigo) from categoria;

-- PROCEDIMENTOS
-- CONSULTA POR NOME DO AUTOR
DELIMITER $$
CREATE PROCEDURE consultaAutorCodigo (IN autorNome varchar(60))
BEGIN
SELECT autores.codigo FROM autores WHERE autores.nome = autorNome;
END $$
DELIMITER ;
CALL consultaAutorCodigo('Isabella');

-- RENOVAÇÂO
DELIMITER $$
CREATE PROCEDURE renovacao(IN data_dev date,IN date_renov date)
BEGIN
UPDATE exemplares_emprestimo set data_prev_devolucao = data_dev;
UPDATE exemplares_emprestimo set data_renovacao = date_renov;
END $$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE validacaoRenov(IN exemplar_cod int)
BEGIN 
SELECT COUNT(cod_emprestimo) FROM emprestimo JOIN exemplares_emprestimo ON (exemplares_emprestimo.cod_emprestimo = emprestimo.codigo)
WHERE cod_exemplar = exemplar_cod
HAVING COUNT(cod_emprestimo)<3;
END $$
DELIMITER ;
 CALL validacaoRenov(1);
 
CREATE OR REPLACE VIEW view_consulta_cat
AS
   SELECT*from categoria;
   
CREATE OR REPLACE VIEW view_consulta_catlivro
	AS  
   SELECT livro.codigo as livro,livro.titulo, categoria.codigo as cat,categoria.descricao
   From livro  JOIN cat_livro ON(cat_livro.cod_livro = livro.codigo) 
   JOIN categoria ON(cat_livro.cod_categoria = categoria.codigo);


DELIMITER &&
CREATE TRIGGER delete_exemplares
AFTER DELETE ON livro FOR EACH ROW
BEGIN
	delete from exemplares where cod.livro = old.codigo;
END; &&
DELIMITER ;

DELIMITER &&
CREATE TRIGGER delete_categorias
BEFORE DELETE ON categorias FOR EACH ROW
BEGIN
	if ( count(select*from cat_livro where cod_categoria = old.codigo) > 0 ) then
    
END; &&
DELIMITER ;

